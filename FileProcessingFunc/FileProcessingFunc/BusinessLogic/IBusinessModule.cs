﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessingFunc.BusinessLogic
{
    public interface IBusinessModule
    {
        Task ProcessFilesAsync();
    }
}
