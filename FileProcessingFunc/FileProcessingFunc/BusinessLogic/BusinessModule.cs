﻿using Azure.Storage.Files.Shares;
using Azure.Storage.Files.Shares.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileProcessingFunc.BusinessLogic
{
    public class BusinessModule : IBusinessModule
    {
        private readonly ILogger _logger;
        public BusinessModule(ILogger<BusinessModule> logger)
        {
            _logger = logger;
        }
        public async Task ProcessFilesAsync()
        {
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("StorageConnectionString");
                string shareName = Environment.GetEnvironmentVariable("AzureFileShareName");
                string pattern = Environment.GetEnvironmentVariable("Pattern");
                ShareClient shareClient = new ShareClient(connectionString, shareName);
                string uploadDirectory = Environment.GetEnvironmentVariable("DirectoryForUplodedFiles");
                ShareDirectoryClient directoryClient = shareClient.GetDirectoryClient(uploadDirectory);

                string regexPattern = ConvertPatternToRegexPattern(pattern);
                Regex regex = new Regex(regexPattern);

                var files = directoryClient.GetFilesAndDirectoriesAsync();

                await foreach (ShareFileItem item in files)
                {
                    if (!item.IsDirectory)
                    {
                        await ProcessFileItem(directoryClient, regex, item);
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Unknown error occured: {ex.Message}");
            }
        }

        private async Task ProcessFileItem(ShareDirectoryClient directoryClient, Regex regex, ShareFileItem item)
        {
            try
            {
                var fileClient = directoryClient.GetFileClient(item.Name);
                bool matchFound = false;

                using (Stream stream = await fileClient.OpenReadAsync())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line = reader.ReadLine();
                        while (line != null)
                        {
                            string[] words = line.Split(new char[] {' ', ',', ';', '.' }, StringSplitOptions.None);
                            if(words.Any(w => regex.IsMatch(w)))
                            {
                                matchFound = true;
                                break;
                            }
                            line = reader.ReadLine();
                        }
                    }
                }

                if (matchFound)
                {
                    await CopyMatchedFileAsync(fileClient);
                }
                await DeleteUnmatchedFilesAsync(fileClient);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Falied to process file {item.Name}.");
                _logger.LogError($"Error details {ex.Message}.");
            }
        }

        private async Task CopyMatchedFileAsync(ShareFileClient matchedFile)
        {
            string connectionString = Environment.GetEnvironmentVariable("StorageConnectionString"); 

            ShareClient shareClient = new ShareClient(connectionString, matchedFile.ShareName);
            string copyDirectory = Environment.GetEnvironmentVariable("DirectoryToCopyFiles");

            ShareDirectoryClient directoryClient = shareClient.GetDirectoryClient(copyDirectory);
            await directoryClient.CreateIfNotExistsAsync();
            
            ShareFileClient destinationFileClient = directoryClient.GetFileClient(matchedFile.Name);

            await destinationFileClient.StartCopyAsync(matchedFile.Uri);
        }

        private async Task DeleteUnmatchedFilesAsync(ShareFileClient unmatchedFile)
        {
            await unmatchedFile.DeleteIfExistsAsync();
        }

        private string ConvertPatternToRegexPattern(string pattern)
        {
            if (!string.IsNullOrEmpty(pattern))
            {
                if (Char.IsLetterOrDigit(pattern[0]))
                {
                    pattern = "^" + pattern;
                }

                if (Char.IsLetterOrDigit(pattern[pattern.Length - 1]))
                {
                    pattern = pattern + "$";
                }

                pattern = pattern.Replace("*", ".*");
                pattern = pattern.Replace("?", ".{1}");
            }

            return pattern;
        }
    }
}

